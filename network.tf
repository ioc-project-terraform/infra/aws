resource "aws_vpc" "vpc_master" {
  provider             = aws.region-master
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "master-vpc-demo"
  }
}

# Get all available AZ's in VPC for master region
data "aws_availability_zones" "azs" {
  provider = aws.region-master
  state    = "available"
}

# Create subnet #1 in eu-west-1
resource "aws_subnet" "subnet_1" {
  provider          = aws.region-master
  availability_zone = data.aws_availability_zones.azs.names[0]
  vpc_id            = aws_vpc.vpc_master.id
  cidr_block        = "192.168.0.0/24"
}

# Create subnet #2 in eu-west-1
resource "aws_subnet" "subnet_2" {
  provider          = aws.region-master
  availability_zone = data.aws_availability_zones.azs.names[1]
  vpc_id            = aws_vpc.vpc_master.id
  cidr_block        = "192.168.1.0/24"
}